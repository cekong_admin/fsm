package common;

public enum FsmOpType {

    ApplyRefund(100),
    CancelRefund(101),
    AgreeRefund(102);

    private Integer code;

    FsmOpType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
