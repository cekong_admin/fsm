package common;

import com.nx.fsm.context.FsmContext;

public class MyContext extends FsmContext {

    private String logStr;
    /**
     * 状态机类型
     */
    private Integer refundType;

    public Integer getRefundType() {
        return refundType;
    }

    public void setRefundType(Integer refundType) {
        this.refundType = refundType;
    }

    public String getLogStr() {
        return logStr;
    }

    public void setLogStr(String logStr) {
        this.logStr = logStr;
    }

    @Override
    public String toString() {
        return "MyContext{" +
                "logStr='" + logStr + '\'' +
                ", refundType=" + refundType +
                '}';
    }
}
