import com.nx.fsm.context.FsmRole;
import common.FsmOpType;
import common.MyContext;
import com.nx.fsm.engine.DefaultFsmEngine;
import bean.FsmSelectorImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.SpringFsmMappingProvider;

/**
 * 服务入口
 */
public class JavaBeanTest {

    @BeforeClass
    public static void before() throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("java-context.xml");
    }

    @Test
    public void javaBean() {
        DefaultFsmEngine engine = new DefaultFsmEngine();
//        engine.setActionProvider(new ActionProviderImpl());
        engine.setFsmSelector(new FsmSelectorImpl());
        engine.setFsmMappingProvider(new SpringFsmMappingProvider());
        engine.init();

        MyContext context = new MyContext();
        context.setRecordId(1001L);
        context.setRefundType(1);
        context.setOpType(FsmOpType.CancelRefund.getCode());
        context.setRole(FsmRole.BUYER);
        context.setStatus(111);
        engine.perform(context);

        System.out.println(engine.getPerformResult(context));
    }


}
