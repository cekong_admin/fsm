package bean;

import com.nx.fsm.context.FsmNodeResult;
import common.AbsOrderAction;
import common.MyContext;

public class CancelRefundHandler extends AbsOrderAction {

    /**
     * 校验参数的方法
     *
     * @param context context
     */
    @Override
    protected void checkParams(MyContext context) {
        System.out.println("=========取消=======");
//        throw new FsmException(ErrorCode.BIZ_ERROR, context);
    }

    /**
     * 执行业务逻辑的方法
     *
     * @param context       context
     * @param fsmNodeResult fsmNodeResult
     */
    @Override
    protected void executeContext(MyContext context, FsmNodeResult fsmNodeResult) {
        System.out.println(context.getRecordId() + "---------取消--------" + fsmNodeResult.getTargetStatus());

    }
}
