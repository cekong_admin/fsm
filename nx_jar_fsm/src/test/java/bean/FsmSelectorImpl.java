package bean;


import com.nx.fsm.context.FsmContext;
import com.nx.fsm.model.FsmSelector;
import common.MyContext;

public class FsmSelectorImpl implements FsmSelector {
    /**
     * 定位流程类型
     *
     * @param context
     * @return
     */
    public Integer selectFsmType(FsmContext context) {

        MyContext myContext = (MyContext) context;

        return myContext.getRefundType();
    }
}
