package bean;

import com.nx.fsm.action.FsmAction;
import com.nx.fsm.action.ActionProvider;
import com.nx.fsm.exception.ErrorCode;
import com.nx.fsm.exception.FsmException;

public class ActionProviderImpl implements ActionProvider {

    /**
     * 构造Action对象
     *
     * @param name
     * @return
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public FsmAction getBean(String name) throws FsmException {
        FsmAction fsmAction = null;
        try {
            fsmAction = (FsmAction) Class.forName(name).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new FsmException(ErrorCode.ACTION_PROVIDER_GET_BEAN, name, e.getMessage());
        }
        return fsmAction;
    }
}
