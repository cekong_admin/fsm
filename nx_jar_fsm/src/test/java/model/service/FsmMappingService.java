package model.service;


import com.nx.fsm.model.FsmMapping;

import java.util.List;

/**
 * 状态机配置
 */
public interface FsmMappingService {


    /**
     * 查询所有有效的状态机配置
     *
     * @return
     */
    List<FsmMapping> getAllEnableConf();
}
