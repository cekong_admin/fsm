package model.service.impl;


import com.nx.fsm.model.FsmMapping;
import model.dao.FsmMappingDao;
import model.service.FsmMappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FsmMappingServiceImpl implements FsmMappingService {

    private static Logger logger = LoggerFactory.getLogger(FsmMappingServiceImpl.class);

    @Autowired
    private FsmMappingDao fsmMappingDao;

    /**
     * 查询所有有效的状态机配置
     *
     * @return
     */
    public List<FsmMapping> getAllEnableConf() {
        return fsmMappingDao.getAllFsmMappings();
    }
}
