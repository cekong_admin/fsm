package model.dao;


import com.nx.fsm.model.FsmMapping;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FsmMappingDao {

    List<FsmMapping> getAllFsmMappings();
}
