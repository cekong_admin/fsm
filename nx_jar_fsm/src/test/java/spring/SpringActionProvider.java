package spring;

import com.nx.fsm.action.ActionProvider;
import com.nx.fsm.action.FsmAction;
import common.SpringContext;

public class SpringActionProvider implements ActionProvider {

    /**
     * 构造Action对象
     *
     * @param name
     * @return
     */
    public FsmAction getBean(String name) {
        return SpringContext.getBean(name);
    }
}
