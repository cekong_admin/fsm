package spring;

import com.nx.fsm.model.FsmMapping;
import com.nx.fsm.model.FsmMappingProvider;
import common.SpringContext;
import model.service.FsmMappingService;

import java.util.List;

public class SpringFsmMappingProvider implements FsmMappingProvider {


    /**
     * 获取状态机配置
     *
     * @return
     */
    public List<FsmMapping> getFsmMapping() {
        FsmMappingService fsmMappingService = SpringContext.getBean(FsmMappingService.class);
        return fsmMappingService.getAllEnableConf();
    }
}
