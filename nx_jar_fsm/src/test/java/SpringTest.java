import com.nx.fsm.context.FsmRole;
import common.FsmOpType;
import common.MyContext;
import com.nx.fsm.engine.DefaultFsmEngine;
import common.SpringContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 服务入口
 */
public class SpringTest {

    @BeforeClass
    public static void before() throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-context.xml");
    }

    @Test
    public void spring() {
        DefaultFsmEngine engine = SpringContext.getBean("defaultFsmEngine");
        MyContext context = new MyContext();
        context.setRefundType(2);
        context.setOpType(FsmOpType.CancelRefund.getCode());
        context.setRole(FsmRole.BUYER);
        context.setStatus(211);
        engine.perform(context);

        System.out.println(engine.getPerformResult(context));
    }

}
