SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_fsm_mapping
-- ----------------------------
DROP TABLE IF EXISTS `t_fsm_mapping`;
CREATE TABLE `t_fsm_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `fsm_type` smallint(6) NOT NULL COMMENT '退款状态机类型',
  `op_type` smallint(6) NOT NULL COMMENT '操作类型',
  `role` smallint(6) NOT NULL COMMENT '用户角色',
  `source_status` smallint(6) NOT NULL COMMENT '源状态',
  `target_status` smallint(6) NOT NULL COMMENT '目标状态',
  `handler` varchar(200) NOT NULL COMMENT '操作实现类',
  `node_type` smallint(6) NOT NULL COMMENT '节点类型,1 begin，2 normal，3 end',
  `enable` smallint(6) NOT NULL DEFAULT '0' COMMENT '有效性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_fsm_mapping
-- ----------------------------
INSERT INTO `t_fsm_mapping` VALUES ('1', '1', '100', '1', '0', '111', 'bean.AgreeRefundHandler', '1', '1');
INSERT INTO `t_fsm_mapping` VALUES ('2', '1', '101', '1', '111', '110', 'bean.CancelRefundHandler', '3', '1');
INSERT INTO `t_fsm_mapping` VALUES ('3', '1', '102', '2', '111', '129', 'bean.AgreeRefundHandler', '3', '1');
INSERT INTO `t_fsm_mapping` VALUES ('11', '2', '101', '1', '211', '220', 'cancelRefundAction', '1', '1');
INSERT INTO `t_fsm_mapping` VALUES ('12', '2', '102', '2', '211', '209', 'agreeRefundAction', '1', '1');
