package com.nx.fsm.model;


import com.nx.fsm.action.FsmAction;
import com.nx.fsm.job.JobConfig;
import com.nx.fsm.transaction.TransactionConfig;

import java.util.HashSet;

/**
 * 状态机配置
 */
public class FsmNode {

    private Integer opType;

    private Integer role;

    private Integer sourceStatus;

    private Integer targetStatus;

    private NodeType nodeType;

    private FsmAction action;

    private HashSet<JobConfig> jobConfigs;

    private HashSet<TransactionConfig> transactionConfigs;

    public FsmNode(Integer opType, Integer role, Integer sourceStatus, Integer targetStatus, NodeType nodeType, FsmAction action) {
        this.opType = opType;
        this.role = role;
        this.sourceStatus = sourceStatus;
        this.targetStatus = targetStatus;
        this.nodeType = nodeType;
        this.action = action;
    }

    public Integer getOpType() {
        return opType;
    }

    public Integer getRole() {
        return role;
    }

    public Integer getSourceStatus() {
        return sourceStatus;
    }

    public Integer getTargetStatus() {
        return targetStatus;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public FsmAction getAction() {
        return action;
    }

    public HashSet<JobConfig> getJobConfigs() {
        return jobConfigs;
    }

    public void setJobConfigs(HashSet<JobConfig> jobConfigs) {
        this.jobConfigs = jobConfigs;
    }

    public HashSet<TransactionConfig> getTransactionConfigs() {
        return transactionConfigs;
    }

    public void setTransactionConfigs(HashSet<TransactionConfig> transactionConfigs) {
        this.transactionConfigs = transactionConfigs;
    }

    @Override
    public String toString() {
        return "FsmNode{" +
                "opType=" + opType +
                ", role=" + role +
                ", sourceStatus=" + sourceStatus +
                ", targetStatus=" + targetStatus +
                ", nodeType=" + nodeType +
                ", action=" + action +
                ", jobConfigs=" + jobConfigs +
                ", transactionConfigs=" + transactionConfigs +
                '}';
    }
}
