package com.nx.fsm.model;

import java.util.List;

/**
 * 状态机配置
 */
public interface FsmMappingProvider {

    /**
     * 获取状态机配置
     *
     * @return
     */
    List<FsmMapping> getFsmMapping();

}
