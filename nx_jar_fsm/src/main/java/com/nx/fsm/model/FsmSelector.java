package com.nx.fsm.model;


import com.nx.fsm.context.FsmContext;

/**
 * 流程选择器
 */
public interface FsmSelector {

    /**
     * 定位流程类型
     *
     * @param context
     * @return
     */
    Integer selectFsmType(FsmContext context);

}
