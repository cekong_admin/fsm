package com.nx.fsm.model;

/**
 * 状态机配置-数据库配置
 */
public class FsmMapping {

    private Integer fsmType;
    private Integer opType;
    private Integer role;
    private Integer sourceStatus;
    private Integer targetStatus;
    private String handler;
    private Integer nodeType;
    //调度任务编号
    private String jobCodes;
    // 事务消息配置
    private String transactionCodes;


    public Integer getFsmType() {
        return fsmType;
    }

    public void setFsmType(Integer fsmType) {
        this.fsmType = fsmType;
    }

    public Integer getOpType() {
        return opType;
    }

    public void setOpType(Integer opType) {
        this.opType = opType;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getSourceStatus() {
        return sourceStatus;
    }

    public void setSourceStatus(Integer sourceStatus) {
        this.sourceStatus = sourceStatus;
    }

    public Integer getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(Integer targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
    }

    public String getJobCodes() {
        return jobCodes;
    }

    public void setJobCodes(String jobCodes) {
        this.jobCodes = jobCodes;
    }

    public String getTransactionCodes() {
        return transactionCodes;
    }

    public void setTransactionCodes(String transactionCodes) {
        this.transactionCodes = transactionCodes;
    }

    @Override
    public String toString() {
        return "FsmMapping{" +
                "fsmType=" + fsmType +
                ", opType=" + opType +
                ", role=" + role +
                ", sourceStatus=" + sourceStatus +
                ", targetStatus=" + targetStatus +
                ", handler='" + handler + '\'' +
                ", nodeType=" + nodeType +
                ", jobCodes='" + jobCodes + '\'' +
                ", transactionCodes='" + transactionCodes + '\'' +
                '}';
    }
}
