package com.nx.fsm.model;

/**
 * 节点类型
 */
public enum NodeType {

    START(1),
    NORMAL(2),
    END(3);

    private Integer id;

    NodeType(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static NodeType getById(Integer id) {
        for (NodeType type : NodeType.values()) {
            if (id.equals(type.getId())) {
                return type;
            }
        }
        return null;
    }
}
