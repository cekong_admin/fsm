package com.nx.fsm.job;

/**
 * 状态机配置-数据库配置
 */
public class JobConfig {

    /**
     * 编码
     */
    private String jobCode;
    /**
     * 操作
     */
    private String opType;
    /**
     * 间隔时间
     */
    private Long intervalTime;

    /**
     * 任务类型，1覆盖模式，2必须执行
     */
    private Integer jobType;

    /**
     * 构造方法，任务类型默认1
     *
     * @param jobCode
     * @param opType
     * @param intervalTime
     */
    public JobConfig(String jobCode, String opType, Long intervalTime) {
        this(jobCode, opType, intervalTime, 1);
    }

    public JobConfig(String jobCode, String opType, Long intervalTime, Integer jobType) {
        this.jobCode = jobCode;
        this.opType = opType;
        this.intervalTime = intervalTime;
        this.jobType = jobType;
    }

    public String getJobCode() {
        return jobCode;
    }

    public String getOpType() {
        return opType;
    }

    public Long getIntervalTime() {
        return intervalTime;
    }

    public Integer getJobType() {
        return jobType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JobConfig jobConfig = (JobConfig)o;
        return jobCode != null ? jobCode.equals(jobConfig.jobCode) : jobConfig.jobCode == null;
    }

    @Override
    public int hashCode() {
        return jobCode != null ? jobCode.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "JobConfig{" + "jobCode='" + jobCode + '\'' + ", opType='" + opType + '\'' + ", intervalTime="
            + intervalTime + ", jobType=" + jobType + '}';
    }
}
