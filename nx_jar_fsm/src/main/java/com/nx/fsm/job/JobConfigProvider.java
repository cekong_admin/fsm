package com.nx.fsm.job;

import java.util.HashSet;

/**
 * 倒计时配置
 */
public interface JobConfigProvider {

    /**
     * 获取倒计时配置
     *
     * @return
     */
    HashSet<JobConfig> getJobConfigs();

}
