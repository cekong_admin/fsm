package com.nx.fsm.transaction.impl;

import com.nx.fsm.context.FsmContext;
import com.nx.fsm.transaction.TransactionConfig;
import com.nx.fsm.transaction.TransactionMsg;
import com.nx.fsm.transaction.TransactionMsgProvider;

import java.util.HashSet;

/**
 * 事务消息
 */
public class DefaultTransactionMsgProvider implements TransactionMsgProvider {

    private static final String ID = "recordId";
    private static final String ROLE = "role";
    private static final String OP = "opType";
    private static final String STATUS = "status";

    /**
     * 获取事务消息
     *
     * @param transactionConfigs
     * @param context
     * @param targetStatus
     * @return
     */
    @Override
    public HashSet<TransactionMsg> getTransactionMsgs(HashSet<TransactionConfig> transactionConfigs, FsmContext context, Integer targetStatus) {
        HashSet<TransactionMsg> transactionMsgs = new HashSet<TransactionMsg>();
        if (transactionConfigs != null) {
            for (TransactionConfig config : transactionConfigs) {
                TransactionMsg transactionMsg = new TransactionMsg(config);
                transactionMsg.getContent().put(ID, String.valueOf(context.getRecordId()));
                transactionMsg.getContent().put(ROLE, String.valueOf(context.getRole().getId()));
                transactionMsg.getContent().put(OP, String.valueOf(context.getOpType()));
                transactionMsg.getContent().put(STATUS, String.valueOf(targetStatus));
                transactionMsgs.add(transactionMsg);
            }
        }
        return transactionMsgs;
    }
}
