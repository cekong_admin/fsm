package com.nx.fsm.transaction;

import java.util.HashSet;

/**
 * 事务消息-配置
 */
public interface TransactionConfigProvider {

    /**
     * 获取事务消息-配置
     *
     * @return
     */
    HashSet<TransactionConfig> getTransactionConfigs();

}
