package com.nx.fsm.transaction;

/**
 * 事务消息配置
 */
public class TransactionConfig {

    private String code;

    private String topic;

    private String tag;

    public TransactionConfig(String code, String topic, String tag) {
        this.code = code;
        this.topic = topic;
        this.tag = tag;
    }

    public String getCode() {
        return code;
    }

    public String getTopic() {
        return topic;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransactionConfig config = (TransactionConfig) o;
        if (code != null ? !code.equals(config.code) : config.code != null) {
            return false;
        }
        return (topic != null ? topic.equals(config.topic) : config.topic == null) && (tag != null ? tag.equals(config.tag) : config.tag == null);
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TransactionConfig{" +
                "code='" + code + '\'' +
                ", topic='" + topic + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }
}
