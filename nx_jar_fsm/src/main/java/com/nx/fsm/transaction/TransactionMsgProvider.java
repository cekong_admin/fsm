package com.nx.fsm.transaction;

import com.nx.fsm.context.FsmContext;

import java.util.HashSet;

/**
 * 事务消息
 */
public interface TransactionMsgProvider {

    /**
     * 获取事务消息
     *
     * @param transactionConfigs 事务配置
     * @param context
     * @param targetStatus       目标状态
     * @return
     */
    HashSet<TransactionMsg> getTransactionMsgs(HashSet<TransactionConfig> transactionConfigs, FsmContext context, Integer targetStatus);

}
