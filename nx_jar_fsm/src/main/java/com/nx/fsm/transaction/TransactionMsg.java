package com.nx.fsm.transaction;

import java.util.HashMap;

/**
 * 事务消息
 */
public class TransactionMsg {

    private String code;

    private String topic;

    private String tag;

    private HashMap<String, String> content = new HashMap<>();

    public TransactionMsg() {
    }

    public TransactionMsg(TransactionConfig config) {
        this.code = config.getCode();
        this.topic = config.getTopic();
        this.tag = config.getTag();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransactionMsg that = (TransactionMsg) o;
        return code != null ? code.equals(that.code) : that.code == null;
    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TransactionMsg{" +
                "code='" + code + '\'' +
                ", topic='" + topic + '\'' +
                ", tag='" + tag + '\'' +
                ", content=" + content +
                '}';
    }
}
