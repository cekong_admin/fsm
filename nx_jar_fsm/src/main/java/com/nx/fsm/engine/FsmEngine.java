package com.nx.fsm.engine;


import com.nx.fsm.context.FsmContext;
import com.nx.fsm.context.FsmNodeResult;

/**
 * 状态机引擎
 *
 * @author pfliu
 */
public interface FsmEngine {

    /**
     * 初始化状态机配置
     */
    void init();

    /**
     * 执行状态机
     *
     * @param context
     */
    void perform(FsmContext context);

    /**
     * 查看状态机运行后状态
     *
     * @param context
     * @return
     */
    FsmNodeResult getPerformResult(FsmContext context);

}
