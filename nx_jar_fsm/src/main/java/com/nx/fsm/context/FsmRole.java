package com.nx.fsm.context;

/**
 * 角色
 */
public enum FsmRole {

    BUYER(1),      //买家
    SELLER(2),     //卖家
    KEFU(3),       //客服
    BROKER(4),     //黄牛
    SYSTEM(9);     //系统调用或离线程序

    private Integer id;

    FsmRole(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static FsmRole getById(Integer id) {
        for (FsmRole type : FsmRole.values()) {
            if (id.equals(type.getId())) {
                return type;
            }
        }
        return null;
    }
}
