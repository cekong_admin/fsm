package com.nx.fsm.context;

import com.nx.fsm.job.JobConfig;
import com.nx.fsm.model.FsmNode;
import com.nx.fsm.model.NodeType;
import com.nx.fsm.transaction.TransactionMsg;

import java.util.HashSet;

public class FsmNodeResult {


    /**
     * 目标状态
     */
    private Integer targetStatus;

    /**
     * 节点类型
     */
    private NodeType nodeType;

    /**
     * 离线配置
     */
    private HashSet<JobConfig> jobConfigs;

    /**
     * 事务配置
     */
    private HashSet<TransactionMsg> transactionMsgs;


    public FsmNodeResult(FsmNode fsmNode) {
        this.targetStatus = fsmNode.getTargetStatus();
        this.nodeType = fsmNode.getNodeType();
        this.jobConfigs = fsmNode.getJobConfigs();
    }


    public Integer getTargetStatus() {
        return targetStatus;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public HashSet<JobConfig> getJobConfigs() {
        return jobConfigs;
    }

    public HashSet<TransactionMsg> getTransactionMsgs() {
        return transactionMsgs;
    }

    public void setTransactionMsgs(HashSet<TransactionMsg> transactionMsgs) {
        this.transactionMsgs = transactionMsgs;
    }


    @Override
    public String toString() {
        return "FsmNodeResult{" +
                "targetStatus=" + targetStatus +
                ", nodeType=" + nodeType +
                ", jobConfigs=" + jobConfigs +
                ", transactionMsgs=" + transactionMsgs +
                '}';
    }
}
