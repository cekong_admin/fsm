package com.nx.fsm.context;

public class FsmContext {

    /**
     * 业务ID
     */
    private Long recordId;

    /**
     * 当前状态
     */
    private Integer status;

    /**
     * 操作
     */
    private Integer opType;

    /**
     * 角色
     */
    private FsmRole role;


    public FsmContext() {
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOpType() {
        return opType;
    }

    public void setOpType(Integer opType) {
        this.opType = opType;
    }

    public FsmRole getRole() {
        return role;
    }

    public void setRole(FsmRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "FsmContext{" +
                "recordId=" + recordId +
                ", status=" + status +
                ", opType=" + opType +
                ", role=" + role +
                '}';
    }
}
