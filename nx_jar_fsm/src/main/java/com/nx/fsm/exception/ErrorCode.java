package com.nx.fsm.exception;

public enum ErrorCode {
    ACTION_PROVIDER_NOT_FOUND(1001, "没有找到ActionProvider实现类."),
    FSM_SELECTOR_NOT_FOUND(1002, "没有找到FsmType选择器."),
    FSM_CONFIT_NOT_FOUND(1003, "没有任何状态机配置项."),
    JOB_PROVIDER_NOT_FOUND(1004, "没有找到JobConfigProvider实现类."),
    MSG_PROVIDER_NOT_FOUND(1005, "没有找到TransactionMsgProvider实现类."),
    FSM_NODE_NOT_FOUND(1011, "状态机缺少配置项, fsmtype: %s, status: %s, role: %s, optype: %s"),
    FSM_TYPE_NOT_FOUND(1012, "状态机类型选择失败, status: %s, role: %s, optype: %s"),
    ACTION_PROVIDER_GET_BEAN(1013, "初始化bean失败, Class: %s, e: %s"),
    FSM_CONFIG_REPEAT(1014, "状态机配置重复, config: %s");

    private int code;
    private String message;

    private ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
