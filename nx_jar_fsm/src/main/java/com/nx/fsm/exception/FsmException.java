package com.nx.fsm.exception;

public class FsmException extends RuntimeException {

    private Integer errorCode;

    private String errorMsg;

    public FsmException(ErrorCode errorCode, Object... arguments) {
        super("code:" + errorCode + " message:" + String.format(errorCode.getMessage(), arguments));
        this.errorCode=errorCode.getCode();
        this.errorMsg = String.format(errorCode.getMessage(), arguments);
    }

    public FsmException(ErrorCode errorCode) {
        super("code:" + errorCode + " message:" + errorCode.getMessage());
        this.errorCode=errorCode.getCode();
        this.errorMsg = errorCode.getMessage();
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
