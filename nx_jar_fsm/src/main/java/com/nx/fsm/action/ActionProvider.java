package com.nx.fsm.action;

import com.nx.fsm.exception.FsmException;

public interface ActionProvider {

    FsmAction getBean(String name) throws FsmException;

}