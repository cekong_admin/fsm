package com.nx.fsm.action.impl;

import com.nx.fsm.action.ActionProvider;
import com.nx.fsm.action.FsmAction;
import com.nx.fsm.exception.ErrorCode;
import com.nx.fsm.exception.FsmException;

/**
 * 默认ActionProvider，反射方式实例化
 *
 * @author pfliu
 */
public class DefaultActionProviderImpl implements ActionProvider {

    /**
     * 构造Action对象
     *
     * @param name
     * @return
     * @throws FsmException
     */
    @Override
    public FsmAction getBean(String name) throws FsmException {
        FsmAction fsmAction = null;
        try {
            fsmAction = (FsmAction) Class.forName(name).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new FsmException(ErrorCode.ACTION_PROVIDER_GET_BEAN, name, e.getMessage());
        }
        return fsmAction;
    }
}
