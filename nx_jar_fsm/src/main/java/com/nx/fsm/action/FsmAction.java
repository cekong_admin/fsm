package com.nx.fsm.action;


import com.nx.fsm.context.FsmContext;
import com.nx.fsm.context.FsmNodeResult;

/**
 * 状态机操作接口
 */
public interface FsmAction {

    /**
     * check: 验证是否通过操作校验，不通过直接抛异常
     *
     * @param context
     */
    void check(FsmContext context);

    /**
     * 执行状态机操作
     *
     * @param context
     * @param fsmNodeResult 状态机结果
     */
    void execute(FsmContext context, FsmNodeResult fsmNodeResult);
}
